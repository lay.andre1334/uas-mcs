package com.example.uas_mcs_app;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Words implements Serializable {

    @SerializedName("word")
    private String name;
    @SerializedName("definitions")
    private ArrayList<Definitions> definitionList;

    public Words(String name, ArrayList<Definitions> definitionList) {
        this.name = name;
        this.definitionList = definitionList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Definitions> getDefinitionList() {
        return definitionList;
    }

    public void setDefinitionList(ArrayList<Definitions> definitionList) {
        this.definitionList = definitionList;
    }
}
