package com.example.uas_mcs_app.ui.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.uas_mcs_app.APIService;
import com.example.uas_mcs_app.Definitions;
import com.example.uas_mcs_app.R;
import com.example.uas_mcs_app.WordAdapter;
import com.example.uas_mcs_app.Words;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ExploreFragment extends Fragment {

    RecyclerView rvWords;
    EditText etSearch;
    ImageButton btnSearch;
    WordAdapter wordAdapter;
    List<Words> wordsList;

    public ExploreFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ExploreFragment newInstance() {
        ExploreFragment fragment = new ExploreFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_explore,container, false);

        etSearch = view.findViewById(R.id.etSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        rvWords = view.findViewById(R.id.rvWords);

        rvWords.setLayoutManager(new LinearLayoutManager(getActivity()));

        wordAdapter = new WordAdapter(getActivity().getApplicationContext());
        rvWords.setAdapter(wordAdapter);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String keyword = etSearch.getText().toString();
                if(!keyword.isEmpty()) Toast.makeText(getActivity(), "Loading...", Toast.LENGTH_SHORT).show();
                else Toast.makeText(getActivity(), "Type a keyword", Toast.LENGTH_SHORT).show();
                searchWords(keyword);

            }
        });
        return view;
    }

    public void searchWords(String keyword){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://myawesomedictionary.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);

        Call<List<Words>> call;
        if(keyword.isEmpty()) {
            call = service.getNone();
        }else{
            call = service.getWords(keyword);
        }

        call.enqueue(new Callback<List<Words>>() {
            @Override
            public void onResponse(Call<List<Words>> call, Response<List<Words>> response) {
                if(response.isSuccessful()){
                    wordsList = response.body();
                    wordAdapter.setWordsList(wordsList);
                }
            }

            @Override
            public void onFailure(Call<List<Words>> call, Throwable t) {
                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
            }
        });

    }

}