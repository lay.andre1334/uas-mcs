package com.example.uas_mcs_app.ui.Fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.uas_mcs_app.APIService;
import com.example.uas_mcs_app.DBHelper;
import com.example.uas_mcs_app.DefinitionAdapter;
import com.example.uas_mcs_app.FavoriteAdapter;
import com.example.uas_mcs_app.FavoriteDB;
import com.example.uas_mcs_app.Favorites;
import com.example.uas_mcs_app.MainActivity;
import com.example.uas_mcs_app.R;
import com.example.uas_mcs_app.Words;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class FavoritesFragment extends Fragment {

    private static RecyclerView rvFavorites;
    private static FavoriteAdapter favoriteAdapter;
    private static FavoriteDB favoriteDB;
    private static ArrayList<Favorites> favoritesArrayList = new ArrayList<>();
    public FavoritesFragment() {
        // Required empty public constructor
    }

    public static FavoritesFragment newInstance() {
        FavoritesFragment fragment = new FavoritesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        favoriteDB = new FavoriteDB(getActivity());
        favoriteAdapter = new FavoriteAdapter(getActivity());
        rvFavorites.setLayoutManager(new LinearLayoutManager(getActivity()));
        getRvData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);

        rvFavorites = view.findViewById(R.id.rvFavorites);
        return view;
    }

    public static void getRvData(){
        favoritesArrayList = favoriteDB.getFavorites();
        favoriteAdapter.setFavoritesArrayList(favoritesArrayList);
        rvFavorites.setAdapter(favoriteAdapter);

    }



}