package com.example.uas_mcs_app;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class DefinitionAdapter extends RecyclerView.Adapter<DefinitionAdapter.ViewHolder> {

    private Context ctx;
    private ArrayList<Definitions> definitionsArrayList;

    public DefinitionAdapter(Context ctx) {
        this.ctx = ctx;
    }

    public void setDefinitionsArrayList(ArrayList<Definitions> definitionsArrayList) {
        this.definitionsArrayList = definitionsArrayList;
    }

    @NonNull
    @NotNull
    @Override
    public DefinitionAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.item_definition, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull DefinitionAdapter.ViewHolder holder, int position) {
            Definitions definition = definitionsArrayList.get(position);

            if(definition.getImgUrl() != null) Glide.with(ctx).load(definition.getImgUrl()).into(holder.ivImg);

            holder.tvType.setText("Type: " + definition.getType());
            holder.tvDefinition.setText("" + definition.getDefinition());
    }

    @Override
    public int getItemCount() {
        return definitionsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvType, tvDefinition;
        ImageView ivImg;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            tvType = itemView.findViewById(R.id.tvType);
            tvDefinition = itemView.findViewById(R.id.tvDefinition);
            ivImg = itemView.findViewById(R.id.ivImg);
        }
    }
}
