package com.example.uas_mcs_app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class FavoriteDB {

    private DBHelper dbHelper;

    public FavoriteDB(Context ctx) {
        dbHelper = new DBHelper(ctx);
    }

    public void insertFavorites(String name){
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(DBHelper.FIELD_FAVORITE_NAME, name);
        db.insert(DBHelper.TABLE_FAVORITES, null, cv);

        db.close();
    }

    public ArrayList<Favorites> getFavorites(){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<Favorites> favoritesArrayList = new ArrayList<>();

        Cursor cursor = db.query(DBHelper.TABLE_FAVORITES, null, null, null, null, null, null);

        while(cursor.moveToNext()){
            int id = cursor.getInt(cursor.getColumnIndex(DBHelper.FIELD_FAVORITE_ID));
            String name = cursor.getString(cursor.getColumnIndex(DBHelper.FIELD_FAVORITE_NAME));

            Favorites favorites = new Favorites(id, name);
            favoritesArrayList.add(favorites);
        }
        cursor.close();
        db.close();
        dbHelper.close();
        return  favoritesArrayList;
    }

    public void deleteFavorites(int favoriteId){
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String selection = DBHelper.FIELD_FAVORITE_ID + "=?";
        String[] selectionArgs = {Integer.toString((favoriteId))};

        db.delete(DBHelper.TABLE_FAVORITES, selection, selectionArgs);

        db.close();
        dbHelper.close();

    }

}
