package com.example.uas_mcs_app;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Definitions implements Serializable {

    @SerializedName("image_url")
    private String imgUrl;
    @SerializedName("type")
    private String type;
    @SerializedName("definition")
    private String definition;

    public Definitions(String imgUrl, String type, String definition) {
        this.imgUrl = imgUrl;
        this.type = type;
        this.definition = definition;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }
}
