package com.example.uas_mcs_app;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "FavoriteDB";
    public static final int DB_VERSION = 1;

    public static final String TABLE_FAVORITES = "Favorite";
    public static final String FIELD_FAVORITE_ID = "FavoriteId";
    public static final String FIELD_FAVORITE_NAME = "FavoriteName";

    public static final String CREATE_FAVORITES_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_FAVORITES +
                    " (" +
                    FIELD_FAVORITE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    FIELD_FAVORITE_NAME + " TEXT" +
                    " )";

    public static final String DROP_FAVORITES_TABLE = "DROP TABLE IF EXISTS " + TABLE_FAVORITES;

    public DBHelper(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_FAVORITES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DROP_FAVORITES_TABLE);
        onCreate(sqLiteDatabase);
    }
}
