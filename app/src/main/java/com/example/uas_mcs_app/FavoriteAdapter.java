package com.example.uas_mcs_app;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.text.PrecomputedText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder> {

    private Context ctx;
    private ArrayList<Favorites> favoritesArrayList;
    private List<Words> wordsList;
    private Words word;
    private FavoriteDB favoriteDB;
    private Intent intent;
    protected static final String SEND_ID = "com.example.uas_mcs_app.SENDWORDS";

    public FavoriteAdapter(Context ctx) {
        this.ctx = ctx;
    }

    public void setFavoritesArrayList(ArrayList<Favorites> favoritesArrayList) {
        this.favoritesArrayList = favoritesArrayList;
        notifyDataSetChanged();
    }

    @NonNull
    @NotNull
    @Override
    public FavoriteAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.item_dictionary_word, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull FavoriteAdapter.ViewHolder holder, int position) {
        Favorites favorites = favoritesArrayList.get(position);
        holder.tvName.setText(favorites.getFavoriteWord());
        holder.btnDelete.setText("DELETE");
        holder.cvWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ctx, "Loading...", Toast.LENGTH_SHORT).show();
                searchWords(favorites.getFavoriteWord());
            }
        });


        favoriteDB = new FavoriteDB(ctx);
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                favoriteDB.deleteFavorites(favorites.getFavoriteId());
                Toast.makeText(ctx, "Word Deleted", Toast.LENGTH_SHORT).show();
                favoritesArrayList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, favoritesArrayList.size());
            }
        });

    }

    @Override
    public int getItemCount() {
        return favoritesArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        Button btnDelete;
        CardView cvWord;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvWord);
            btnDelete = itemView.findViewById(R.id.button);
            cvWord = itemView.findViewById(R.id.cvWord);
        }
    }

    public void searchWords(String keyword){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://myawesomedictionary.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);

        Call<List<Words>> call = service.getWords(keyword);

        call.enqueue(new Callback<List<Words>>() {
            @Override
            public void onResponse(Call<List<Words>> call, Response<List<Words>> response) {
                if(response.isSuccessful()){
                    wordsList = response.body();
                    word = wordsList.get(0);
                    intent = new Intent(ctx, WordDetailActivity.class);
                    intent.putExtra(SEND_ID, word);
                    ctx.startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<List<Words>> call, Throwable t) {
                Toast.makeText(ctx, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
