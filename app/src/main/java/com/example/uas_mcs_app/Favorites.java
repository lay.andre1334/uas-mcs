package com.example.uas_mcs_app;

public class Favorites {

    int favoriteId;
    String favoriteWord;

    public Favorites(int favoriteId, String favoriteWord) {
        this.favoriteId = favoriteId;
        this.favoriteWord = favoriteWord;
    }

    public int getFavoriteId() {
        return favoriteId;
    }

    public void setFavoriteId(int favoriteId) {
        this.favoriteId = favoriteId;
    }

    public String getFavoriteWord() {
        return favoriteWord;
    }

    public void setFavoriteWord(String favoriteWord) {
        this.favoriteWord = favoriteWord;
    }
}
