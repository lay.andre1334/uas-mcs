package com.example.uas_mcs_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class WordDetailActivity extends AppCompatActivity {

    protected static final String SEND_ID = "com.example.uas_mcs_app.SENDWORDS";

    TextView tvTitleWord;
    Button btnSaveDetail;
    Words words;
    RecyclerView rvDefinition;
    DefinitionAdapter definitionAdapter;

    FavoriteDB favoriteDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_detail);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        tvTitleWord = findViewById(R.id.tvTitleWord);
        btnSaveDetail = findViewById(R.id.btnSaveDetail);
        rvDefinition = findViewById(R.id.rvDefinition);
        Intent intent = getIntent();
        words = (Words) intent.getSerializableExtra(SEND_ID);

        tvTitleWord.setText("Word: " + words.getName());
        getRvData();

        favoriteDB = new FavoriteDB(this);
        btnSaveDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateName(words.getName())){
                    favoriteDB.insertFavorites(words.getName());
                    Toast.makeText(getApplicationContext(), "Word saved", Toast.LENGTH_SHORT).show();
                }else Toast.makeText(getApplicationContext(), "Already Saved", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void getRvData(){
        definitionAdapter = new DefinitionAdapter(this);
        definitionAdapter.setDefinitionsArrayList(words.getDefinitionList());

        rvDefinition.setAdapter(definitionAdapter);
        rvDefinition.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    public boolean validateName(String word){
        ArrayList<Favorites> favoritesArrayList = favoriteDB.getFavorites();
        for(int i = 0 ; i<favoritesArrayList.size() ; i++){
            if(word.equals(favoritesArrayList.get(i).getFavoriteWord())) return false;
            Log.i("testing", "masuk");
        }
        return true;
    }

}