package com.example.uas_mcs_app;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.uas_mcs_app.ui.Fragments.FavoritesFragment;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class WordAdapter extends RecyclerView.Adapter<WordAdapter.ViewHolder> {

    private Context ctx;
    private List<Words> wordsList = new ArrayList<>();
    private FavoriteDB favoriteDB;
    protected static final String SEND_ID = "com.example.uas_mcs_app.SENDWORDS";

    public WordAdapter(Context ctx) {
        this.ctx = ctx;
    }

    public void setWordsList(List<Words> wordsList) {
        this.wordsList = wordsList;
        notifyDataSetChanged();
    }



    @NotNull
    @Override
    public WordAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.item_dictionary_word, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull WordAdapter.ViewHolder holder, int position) {
        Words word = wordsList.get(position);
        holder.tvName.setText(word.getName());
        holder.btnSave.setText("SAVE");
        holder.cvWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ctx, WordDetailActivity.class);
                intent.putExtra(SEND_ID, word);
                ctx.startActivity(intent);
            }
        });

        favoriteDB = new FavoriteDB(ctx);
        holder.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateName(word.getName())){
                    favoriteDB.insertFavorites(word.getName());
                    Toast.makeText(ctx, "Word saved", Toast.LENGTH_SHORT).show();
                    FavoritesFragment.getRvData();
                }else Toast.makeText(ctx, "Already Saved", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return wordsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        Button btnSave;
        CardView cvWord;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tvWord);
            btnSave = itemView.findViewById(R.id.button);
            cvWord = itemView.findViewById(R.id.cvWord);
        }
    }

    public boolean validateName(String word){
        ArrayList<Favorites> favoritesArrayList = favoriteDB.getFavorites();
        for(int i = 0 ; i<favoritesArrayList.size() ; i++){
            if(word.equals(favoritesArrayList.get(i).getFavoriteWord())) return false;
            Log.i("testing", "masuk");
        }
        return true;
    }
}
