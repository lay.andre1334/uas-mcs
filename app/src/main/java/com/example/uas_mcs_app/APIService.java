package com.example.uas_mcs_app;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {

    @GET("words")
    Call<List<Words>> getWords(@Query("q") String keyword);

    @GET("words")
    Call<List<Words>> getNone();

}
